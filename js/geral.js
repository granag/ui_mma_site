(function() {



	$(document).ready(function(){

		$('.pg-servicos section.servico-destaque nav.servico-destaque-menu ul li').click(function(){
			event.preventDefault();
			$('.pg-servicos section.servico-destaque nav.servico-destaque-menu ul li').removeClass("ativo");
			$('.pg-servicos .areaServicos').removeClass("ativo");
			$(this).addClass("ativo");
			let dataId = $(this).attr('data-id');
			$("#"+dataId).addClass("ativo");
			
			
		});

		$('.carrossel-servicos').owlCarousel({
			items : 3,
			dots: false,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			center: false ,
			margin: 15,
			autoWidth: true,
			// responsiveClass:true,			    
			// responsive:{
			// 	320:{
			// 		items:1,
			// 		margin: 15,
			// 	},
			// 	425:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	500:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	768:{
			// 		items:3,
			// 		margin: 30,
			// 	},
			// 	850:{
			// 		items:4,
			// 	},
			// }
		});

		$('.carrossel-projeto').owlCarousel({
			items : 1,
			dots: false,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			center: true,
			autoWidth: true,
			// responsiveClass:true,			    
			// responsive:{
			// 	320:{
			// 		items:1,
			// 		margin: 15,
			// 	},
			// 	425:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	500:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	768:{
			// 		items:3,
			// 		margin: 30,
			// 	},
			// 	850:{
			// 		items:4,
			// 	},
			// }
		});

		$('.pg-projetos .menu-filtro .filtros .modelo img.grid').click(function(){
			$('.pg-projetos .secao-projetos .lista-projetos').addClass('grid-projetos');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-md-4').addClass('col-md-12');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-md-12').removeClass('col-md-4');
			$('.pg-projetos .secao-projetos .grid-projetos li:nth-child(3n+2)').addClass('evento-centro');
			$('.pg-projetos .menu-filtro .filtros .modelo img').removeClass('modelo-active');
			$(this).addClass('modelo-active');
		});

		$('.pg-projetos .menu-filtro .filtros .modelo img.list').click(function(){
			$('.pg-projetos .secao-projetos .lista-projetos').removeClass('grid-projetos');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-md-12').addClass('col-md-4');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-md-4').removeClass('col-md-12');
			$('.pg-projetos .secao-projetos .grid-projetos li').removeClass('evento-centro');
			$('.pg-projetos .menu-filtro .filtros .modelo img').removeClass('modelo-active');
			$(this).addClass('modelo-active');
		});

		$('.pg .secao-localizacao ul li').click(function(){
			let myThisUl = $(this).children('article').children('ul');

			$(this).toggleClass('localizacao-active');

			setTimeout(function(){
				myThisUl.toggleClass('active-sobre');
				myThisUl.toggleClass('show-sobre');
			}, 400);

		});
		

	});



}())